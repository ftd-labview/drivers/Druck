﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="15008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Documentation" Type="Folder">
			<Item Name="920_178a.pdf" Type="Document" URL="../Documentation/920_178a.pdf"/>
			<Item Name="DPI142-150SCPImanual.pdf" Type="Document" URL="../Documentation/DPI142-150SCPImanual.pdf"/>
			<Item Name="DPI142.pdf" Type="Document" URL="../Documentation/DPI142.pdf"/>
		</Item>
		<Item Name="Query - Pressure.vi" Type="VI" URL="//Bt4167/LabVIEW/LabVIEW/Drivers/Druck/Query - Pressure.vi"/>
		<Item Name="Query - Unit.vi" Type="VI" URL="//Bt4167/LabVIEW/LabVIEW/Drivers/Druck/Query - Unit.vi"/>
		<Item Name="Query - Error.vi" Type="VI" URL="../Query - Error.vi"/>
		<Item Name="Open.vi" Type="VI" URL="//Bt4167/LabVIEW/LabVIEW/Drivers/Druck/Open.vi"/>
		<Item Name="test.vi" Type="VI" URL="../test.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
			</Item>
			<Item Name="user.lib" Type="Folder">
				<Item Name="Trim Whitespace__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace__ogtk.vi"/>
				<Item Name="Trim Whitespace (String)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String)__ogtk.vi"/>
				<Item Name="Trim Whitespace (String Array)__ogtk.vi" Type="VI" URL="/&lt;userlib&gt;/_OpenG.lib/string/string.llb/Trim Whitespace (String Array)__ogtk.vi"/>
			</Item>
			<Item Name="Query comport.vi" Type="VI" URL="../Query comport.vi"/>
			<Item Name="Query - Pressure.vi" Type="VI" URL="../Query - Pressure.vi"/>
			<Item Name="Open.vi" Type="VI" URL="../Open.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
