# Druck DPI-142 barometric indicator

LabVIEW drivers and test files.



## Software Requirements

LabVIEW 2015 or later

## License

This project is licenced under the [EUPL Licence](LICENCE.md).

## Contact

For any questions or inquiries, please contact Dirk Geerts (d.j.m.geerts@tudelft.nl).
